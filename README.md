# foss-otg-phone


### The future of this page will be more visually appealing landing page with documentation and howtos linked.


## base mobile OS

[GrapheneOS](https://grapheneos.org)


## Apps List

Found at my blog post at https://craignuzzo.tech/grapheneos


## Goals
* Install something (i.e. package, config file, etc.) once and be portable
* Limit phone usage entirely
* No Google or other trackers


## Additional Resources
Great place to find privacy respecting apps is at [PrivacyTools.io Software page](https://www.privacytools.io/software/)

F-Droid repos can be found [here](https://gitlab.com/fdroid).
